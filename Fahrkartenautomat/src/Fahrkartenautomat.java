﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static Scanner tastatur = new Scanner(System.in);

	public static double fahrkartenbestellungErfassen() {
		double zuZahlenderBetrag;
		int ticketAnzahl;

		String[] ticketNamen = { "Einzelfahrschein Berlin AB [2,90 EUR] (1)",
				"Einzelfahrschein Berlin BC [3,30 EUR] (2)",
				"Einzelfahrschein Berlin ABC [3,60 EUR] (3)",
				"Kurzstrecke [1,90 EUR] (4)",
				"Tageskarte Berlin AB [8,60 EUR] (5)",
				"Tageskarte Berlin BC [9,00 EUR] (6)", 
				"Tageskarte Berlin ABC [9,60 EUR] (7)",
				"Kleingruppen-Tageskarte Berlin AB [23,50 EUR] (8)",
				"Kleingruppen-Tageskarte Berlin BC [24,30 EUR] (9)",
				"Kleingruppen-Tageskarte Berlin ABC [24,90 EUR] (19)" };
		double[] ticketPreise = { 290, 330, 360, 190, 860, 900, 960, 2350, 2430, 2490 };

		for (int i = 0; i < ticketNamen.length; i++) {
			System.out.println(ticketNamen[i]);
		}
		boolean isCorrect = true;
		int ticketAuswahl = 0;
		while (isCorrect) {
			ticketAuswahl = tastatur.nextInt();
			if (ticketAuswahl < 1 || ticketAuswahl > ticketNamen.length) {
				System.out.println("Ungültige Eingabe. Bitte geben Sie eine gültige Ticketnummer ein:");
				continue;
			} else {
				isCorrect = false;
			}
		}
		zuZahlenderBetrag = ticketPreise[ticketAuswahl - 1];

		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextInt();

		if (ticketAnzahl > 10 || ticketAnzahl < 1) {
			ticketAnzahl = 1;
			System.out.println(
					"Ungültige Eingabe. Sie können maximal zehn Tickets gleichzeitig kaufen und die Ticketanzahl kann nicht negativ sein. Die Anzahl der Tickets wurde auf 1 gesetzt.");
		}

		zuZahlenderBetrag = ticketAnzahl * zuZahlenderBetrag;

		return zuZahlenderBetrag;
	}

	public static String[] muenzeAusgeben(int betrag, String einheit) {
		String[] ret = { "   ****   ", String.format("  * %2s *  ", betrag), String.format(" * %s * ", einheit),
				"  *    *  ", "   ****   " };
		return ret;
	}

	public static int fahrkartenBezahlen(int zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		int rückgabebetrag;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag)/100);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze*100;
		}
		rückgabebetrag = (int) eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}

	public static void warte(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void fahrkartenAusgabe() {

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(500);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(int rückgabebetrag) {

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		if (rückgabebetrag > 0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", ((double) rückgabebetrag)/100);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			int row = 0;
			int[] existierendeMünzen = { 200, 100, 50, 20, 10, 5 };
			String[] rowArray = { "", "", "", "", "" };

			for (double wert : existierendeMünzen) {
				String währung = "Cent";
				double displaywert = wert;
				if (wert >= 100) {
					währung = "Euro";
					displaywert *= 0.01;
				}
				System.out.println("Checking for "+displaywert+" ("+wert+") "+währung);
				while (rückgabebetrag >= wert) {
					
					String[] current = muenzeAusgeben((int) Math.round(displaywert), währung);
					for (int i = 0; i < rowArray.length; i++) {
						rowArray[i] = rowArray[i] + current[i];
						if (row > 2) {
							rowArray[i] = rowArray[i] + "\n";
						}
					}
					rückgabebetrag -= wert;
					row++;
					if (row > 2 ) {
						for (String line : rowArray) {
							System.out.println(line);
						}
						String[] empty = { "", "", "", "", "" };
						rowArray = empty;
						row = 0;
					}
				}
				if(wert == 5) {
					for (String line : rowArray) {
						System.out.println(line);
					}
				}
				System.out.println("r "+rückgabebetrag);
				System.out.println("w "+wert);
				System.out.println(rückgabebetrag >= wert);
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

		warte(5500);

	}

	public static void main(String[] args) {
		while (true) {
			double zahlbetrag = fahrkartenbestellungErfassen();
			int rückgabe = fahrkartenBezahlen((int) zahlbetrag);
			fahrkartenAusgabe();
			rueckgeldAusgeben(rückgabe);
		}
	}
}