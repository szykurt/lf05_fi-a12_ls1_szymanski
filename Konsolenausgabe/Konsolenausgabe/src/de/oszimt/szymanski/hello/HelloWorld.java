package de.oszimt.szymanski.hello;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Das ist ein Beispielsatz.\n"+"Ein Beispielsatz ist das.\n");
		// PrintLn appends a newline character to the output while print does not.
		String str = "***************";
		System.out.printf("%8.1s\n", str);
		System.out.printf("%9.3s\n", str);
		System.out.printf("%10.5s\n", str);
		System.out.printf("%11.7s\n", str);
		System.out.printf("%12.9s\n", str);
		System.out.printf("%13.11s\n", str);
		System.out.printf("%14.13s\n", str);
		System.out.printf("%5s\n", str);
		System.out.printf("%9.3s\n", str);
		System.out.printf("%9.3s\n", str);
		
		double a = 22.4234234; 
		double b = 111.2222;
		double c = 4.0; 
		double d = 1000000.551;
		double e = 97.34;

		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
	}

}
