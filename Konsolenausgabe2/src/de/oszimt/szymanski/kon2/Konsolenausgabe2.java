package de.oszimt.szymanski.kon2;

public class Konsolenausgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.printf("%5s\n", "**");
		System.out.printf("%s%7s\n", "*", "*");
		System.out.printf("%s%7s\n", "*", "*");
		System.out.printf("%5s\n", "**");
		
		

		System.out.printf("0!\t= \t\t\t= %4s\n", "1");
		System.out.printf("1!\t= 1\t\t\t= %4s\n", "1");
		System.out.printf("2!\t= 1 * 2\t\t\t= %4s\n", "2");
		System.out.printf("3!\t= 1 * 2 * 3\t\t= %4s\n", "6");
		System.out.printf("4!\t= 1 * 2 * 3 * 4\t\t= %4s\n", "24");
		System.out.printf("5!\t= 1 * 2 * 3 * 4 * 5\t= %4s\n", "120");
		
		
		

		System.out.printf("Fahrenheit\t| Celsius\n");
		System.out.printf("-------------------------\n");
		System.out.printf("%+d\t\t| %6.2f \n", -20,  -28.8889);
		System.out.printf("%+d\t\t| %6.2f \n", -10, -23.3333);
		System.out.printf("%+d\t\t| %6.2f \n", 0, -17.7778);
		System.out.printf("%+d\t\t| %6.2f \n", 20, -6.6667);
		System.out.printf("%+d\t\t| %6.2f \n", 30, -1.1111);
	}

}
