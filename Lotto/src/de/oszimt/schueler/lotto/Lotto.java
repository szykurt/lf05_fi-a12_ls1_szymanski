package de.oszimt.schueler.lotto;

public class Lotto {

	public static void main(String[] args) {

		int[] a = {3, 7, 12, 18, 37, 42};
		System.out.print("[ ");
		for(int e : a) {
			System.out.printf(e+" ");
		}
		System.out.print("]\n");
		
		if(arrayContains(a, 12)) {
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		}
		if(!arrayContains(a, 13)) {
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}
	
	public static boolean arrayContains(int[] a, int z) {
		for(int e : a) {
			if(e == z) {
				return true;
			}
		}
		return false;
	}

}
