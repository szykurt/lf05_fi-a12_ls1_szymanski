package de.oszimt.szymanski.uebung;

import java.util.Scanner;

public class Hallo {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);  
	    System.out.print("Bitte geben Sie ihren Namen ein: "); 
	    
	    String name = myScanner.next();
	    
	    System.out.print("Hallo "+name+". Bitte geben Sie nun ihr Alter ein: ");
	    
	    int age = myScanner.nextInt();
	    
	    System.out.println("Ok. "+name+" ist "+age+" Jahre alt.");
	}

}
